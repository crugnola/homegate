package it.sephiroth.homegate

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import it.sephiroth.homegate.viewModels.MainViewModel
import org.junit.Assert
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.CountDownLatch

@RunWith(AndroidJUnit4::class)
class MainViewModelAndroidTest {

    @Rule
    @JvmField
    public val activity = ActivityTestRule<TestActivity>(TestActivity::class.java)

    private lateinit var viewModel: MainViewModel

    @Before
    fun init() {
        viewModel = ViewModelProviders.of(activity.activity).get(MainViewModel::class.java)
    }

    @Test
    fun testIsEmpty() {
        assertNotNull(viewModel)
        assertNull(viewModel.propertyListing.value)
    }

    @Test
    fun testIsLoading() {
        val latch = CountDownLatch(2)
        var loading = false

        viewModel
                .propertyListingLoader
                .doOnNext { t ->
                    run {
                        Assert.assertNotEquals(loading, t)
                        loading = t
                        latch.countDown()
                    }
                }
                .subscribe()

        viewModel.gatherInitialValue { }

        latch.await()
    }

    @Test
    fun testContents() {
        val latch = CountDownLatch(1)

        viewModel.propertyListing.observe(activity.activity, Observer { _ ->
            run {
                latch.countDown()
            }
        })
        viewModel.gatherInitialValue { err ->
            run {
                throw err
                latch.countDown()
            }
        }

        latch.await()

        assertNotNull(viewModel.propertyListing.value)
    }
}