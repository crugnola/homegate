package it.sephiroth.homegate

import android.graphics.*
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import it.sephiroth.homegate.utils.BlurBitmapCreator
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import java.util.concurrent.CountDownLatch

@RunWith(AndroidJUnit4::class)
class BitmapCreatorAndroidTest {

    private val creator = BlurBitmapCreator.getInstance(InstrumentationRegistry.getTargetContext())

    @Test
    fun testBlurBitmap() {
        val latch = CountDownLatch(1)
        val input = createBitmap(10, 10, Color.BLACK)

        creator.blurImage(input.second, input.first, { bitmap: Bitmap, key: String ->
            run {
                latch.countDown()
                Assert.assertEquals(input.first, key)
                Assert.assertNotNull(bitmap)
            }
        }, { e: Throwable, key: String ->
            run {
                latch.countDown()
                throw e
            }
        })

        latch.await()
    }

    @Test
    fun testCache() {
        var latch = CountDownLatch(1)
        val input = createBitmap(10, 10, Color.BLACK)
        var generatedBitmap: Bitmap? = null
        var generatedKey: String? = null

        creator.blurImage(input.second, input.first, { bitmap: Bitmap, key: String ->
            run {
                latch.countDown()
                Assert.assertEquals(input.first, key)
                Assert.assertNotNull(bitmap)

                generatedBitmap = bitmap
                generatedKey = key

            }
        }, { e: Throwable, key: String ->
            run {
                latch.countDown()
                throw e
            }
        })

        latch.await()

        latch = CountDownLatch(1)

        creator.blurImage(input.second, input.first, { bitmap: Bitmap, key: String ->
            run {
                latch.countDown()
                Assert.assertEquals(input.first, key)
                Assert.assertNotNull(bitmap)

                Assert.assertEquals(generatedBitmap, bitmap)
                Assert.assertEquals(generatedKey, key)

            }
        }, { e: Throwable, key: String ->
            run {
                latch.countDown()
                throw e
            }
        })

        latch.await()
    }

    fun createBitmap(w: Int, h: Int, color: Int): Pair<String, Bitmap> {
        val bmp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bmp)
        val paint = Paint()
        paint.color = color
        canvas.drawRect(Rect(0, 0, w, h), paint)

        return Pair(UUID.randomUUID().toString(), bmp)
    }
}