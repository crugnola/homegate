package it.sephiroth.homegate

import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
class FavouritesTest {

    private val favourites = Favourites.getInstance(RuntimeEnvironment.application.applicationContext)

    @Before
    fun init() {
        favourites.clearAll()
    }

    @Test
    fun testEmpty() {
        assertFalse(favourites.isFavourite(0))
    }

    @Test
    fun testInsert() {
        favourites.addToFavourites(0)
        assertTrue(favourites.isFavourite(0))
        assertFalse(favourites.isFavourite(1))

        favourites.addToFavourites(1)
        assertTrue(favourites.isFavourite(1))
    }

    @Test
    fun testRemoveInvalid() {
        assertFalse(favourites.isFavourite(0))
        favourites.removeFromFavourites(0)
        assertFalse(favourites.isFavourite(0))
    }

    @Test
    fun testRemove() {
        favourites.addToFavourites(1)
        favourites.addToFavourites(2)
        assertTrue(favourites.isFavourite(1))
        assertTrue(favourites.isFavourite(2))

        favourites.removeFromFavourites(1)
        assertFalse(favourites.isFavourite(1))
        assertTrue(favourites.isFavourite(2))

        favourites.removeFromFavourites(2)
        assertFalse(favourites.isFavourite(2))
    }

    @Test
    fun testClear() {
        favourites.addToFavourites(1)
        favourites.addToFavourites(2)
        favourites.addToFavourites(3)
        favourites.addToFavourites(4)

        assertTrue(favourites.isFavourite(1))
        assertTrue(favourites.isFavourite(2))
        assertTrue(favourites.isFavourite(3))
        assertTrue(favourites.isFavourite(4))

        favourites.clearAll()

        assertFalse(favourites.isFavourite(1))
        assertFalse(favourites.isFavourite(2))
        assertFalse(favourites.isFavourite(3))
        assertFalse(favourites.isFavourite(4))
    }

}