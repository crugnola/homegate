package it.sephiroth.homegate.viewModels

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.PublishSubject
import it.sephiroth.homegate.api.HomegateApiService
import it.sephiroth.homegate.api.Models

class MainViewModel(application: Application) : AndroidViewModel(application) {

    var propertyListing: LiveData<Models.Properties> = MutableLiveData()
    var propertyListingLoader: PublishSubject<Boolean> = PublishSubject.create()


    fun gatherInitialValue(err: ((t: Throwable) -> Unit)?) {
        if (propertyListing.value == null) {
            return forceReload(err)
        }
    }

    @SuppressLint("CheckResult")
    fun forceReload(err: ((t: Throwable) -> Unit)?) {
        HomegateApiService.service
                .queryProperties()
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { propertyListingLoader.onNext(true) }
                .doOnTerminate { propertyListingLoader.onNext(false) }
                .subscribe(
                        { it -> (propertyListing as MutableLiveData).postValue(it) },
                        { e -> run { err?.invoke(e) } })

    }
}
