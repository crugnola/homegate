package it.sephiroth.homegate.api

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET

interface HomegateApiService {

    @GET("properties")
    fun queryProperties(): Observable<Models.Properties>

    companion object {
        private val BASE_URL = "http://private-492e5-homegate1.apiary-mock.com/"

        val service by lazy {
            HomegateApiService.create()
        }

        private fun create(): HomegateApiService {
            val builder = Retrofit.Builder()
                    .addConverterFactory(MoshiConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.computation()))
                    .baseUrl(BASE_URL)
                    .build()
            return builder.create(HomegateApiService::class.java)
        }
    }
}