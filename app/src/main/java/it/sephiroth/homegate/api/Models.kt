package it.sephiroth.homegate.api

object Models {

    data class ExternalUrls(val url: String, val type: String, val label: String)

    data class Properties(val resultCount: Int,
                          val start: Int,
                          val page: Int,
                          val pageCount: Int,
                          val itemsPerPage: Int,
                          val hasNextPage: Boolean,
                          val hasPreviousPage: Boolean,
                          val items: List<Item>)

    data class Item(val advertisementId: Long,
                    val score: Int,
                    val agencyId: String,
                    val title: String,
                    val street: String,
                    val zip: String,
                    val text: String,
                    val city: String,
                    val country: String,
                    val geoLocation: String,
                    val offerType: String,
                    val objectCategory: String,
                    val objectType: Int,
                    val numberRooms: Float,
                    val floor: Int,
                    val surfaceLiving: Int,
                    val surfaceUsable: Int,
                    val currency: String,
                    val sellingPrice: Int,
                    val price: Int,
                    val priceUnit: String,
                    val timestamp: Long,
                    val timestampStr: String,
                    val balcony: Boolean,
                    val lift: Boolean,
                    val wheelchairAccess: Boolean,
                    val animalAllowed: Boolean,
                    val builtNew: Boolean,
                    val lastModified: Long,
                    val countryLabel: String,
                    val floorLabel: String,
                    val description: String,
                    val listingType: String,
                    val objectTypeLabel: String,
                    val agencyLogoUrl: String,
                    val agencyPhoneDay: String,
                    val contactPerson: String,
                    val contactPhone: String,
                    val interestedFormType: Int,
                    val picFilename1Small: String,
                    val picFilename1Medium: String,
                    val pictures: List<String>,
                    val externalUrls: List<ExternalUrls>) {

        val humanAddress: String
            get() {
                var result = street
                if (zip.isNotEmpty()) result += if (result.isNotEmpty()) ", $zip" else zip
                if (city.isNotEmpty()) result += if (result.isNotEmpty()) " $city" else city
                return result
            }

        val gmapsLocationQuery: String
            get() {
                return geoLocation.split(",").reversed().joinToString(",")
            }

        val headPicture: String
            get() {
                return if (pictures.isNotEmpty()) pictures[0] else (if (picFilename1Medium.isEmpty()) picFilename1Small else picFilename1Medium)
            }

        val monthlyPrice: String
            get() {
                return if (price > 0) {
                    "$price $currency"
                } else {
                    ""
                }
            }
    }
}
