package it.sephiroth.homegate.utils

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Bitmap
import android.renderscript.Allocation
import android.renderscript.Element
import android.renderscript.RenderScript
import android.renderscript.ScriptIntrinsicBlur
import android.support.v4.util.LruCache
import io.reactivex.Single
import io.reactivex.SingleOnSubscribe
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class BlurBitmapCreator private constructor(context: Context) {

    private val maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
    private val cacheSize = maxMemory / 16
    private val memoryCache = object : LruCache<String, Bitmap>(cacheSize) {
        override fun sizeOf(key: String, bitmap: Bitmap): Int {
            return bitmap.byteCount / 1024
        }
    }
    private val rsScript = RenderScript.create(context)
    private val blur = ScriptIntrinsicBlur.create(rsScript, Element.U8_4(rsScript))


    @SuppressLint("CheckResult")
    fun blurImage(input: Bitmap,
                  key: String,
                  complete: (bitmap: Bitmap, key: String) -> Unit,
                  error: (e: Throwable, key: String) -> Unit) {
        Single.create(SingleOnSubscribe<Bitmap> { emitter ->
            run {
                try {
                    val cached = memoryCache.get(key)
                    cached?.let {
                        Timber.d("bitmap from cache!")
                        emitter.onSuccess(cached)
                        return@SingleOnSubscribe
                    }

                    val alloc = Allocation.createFromBitmap(rsScript, input)
                    blur.setRadius(12f)
                    blur.setInput(alloc)

                    val result = Bitmap.createBitmap(input.width / 4, input.height / 4, Bitmap.Config.ARGB_8888)
                    val outAlloc = Allocation.createFromBitmap(rsScript, result)

                    blur.forEach(outAlloc)
                    outAlloc.copyTo(result)
                    rsScript.destroy()

                    Timber.d("bitmap generated!")
                    memoryCache.put(key, result)
                    emitter.onSuccess(result)
                } catch (e: java.lang.Exception) {
                    emitter.onError(e)
                }
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe({ t -> complete(t, key) }, { t -> error(t, key) })
    }

    companion object {

        @Volatile
        private var instance: BlurBitmapCreator? = null

        fun getInstance(context: Context): BlurBitmapCreator {
            val i = instance
            if (i != null) {
                return i
            }

            return synchronized(this) {
                val i2 = instance
                if (i2 != null) {
                    i2
                } else {
                    val created = BlurBitmapCreator(context)
                    instance = created
                    created
                }
            }
        }
    }
}