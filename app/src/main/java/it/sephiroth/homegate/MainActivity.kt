package it.sephiroth.homegate

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.support.v4.content.ContextCompat.startActivity
import android.support.v4.view.ViewPager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.recyclerview.extensions.AsyncListDiffer
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.rd.PageIndicatorView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import it.sephiroth.homegate.api.Models
import it.sephiroth.homegate.utils.BlurBitmapCreator
import it.sephiroth.homegate.viewModels.MainViewModel
import timber.log.Timber

class MainActivity : AppCompatActivity() {

    @BindView(R.id.swipeLayout)
    lateinit var swipeLayout: SwipeRefreshLayout

    @BindView(R.id.recyclerView)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    @BindView(R.id.retryButton)
    lateinit var retryButton: Button

    @BindView(R.id.networkErrorView)
    lateinit var networkErrorView: View

    private lateinit var viewModel: MainViewModel

    private lateinit var adapter: Adapter

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        setSupportActionBar(toolbar)

        adapter = Adapter(this)
        recyclerView.adapter = adapter

        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        viewModel.propertyListing.observe(this, Observer { result ->
            run {
                adapter.submitList(result?.items)
            }
        })

        viewModel.propertyListingLoader.subscribe { isLoading ->
            run {
                swipeLayout.isRefreshing = isLoading
                if (isLoading && networkErrorView.visibility == View.VISIBLE) {
                    networkErrorView.visibility = if (adapter.itemCount == 0) View.VISIBLE else View.GONE
                } else {
                    networkErrorView.visibility = View.GONE
                }
                recyclerView.visibility = View.VISIBLE
            }
        }

        swipeLayout.setOnRefreshListener { loadInitialData() }
        viewModel.gatherInitialValue { t -> onErrorLoadingListing(t) }
        retryButton.setOnClickListener { loadInitialData() }
    }

    private fun loadInitialData() {
        viewModel.forceReload { t -> onErrorLoadingListing(t) }
    }

    private fun onErrorLoadingListing(error: Throwable) {
        Toast.makeText(this, error.localizedMessage, Toast.LENGTH_SHORT).show()
        networkErrorView.visibility = if (adapter.itemCount == 0) View.VISIBLE else View.GONE
        recyclerView.visibility = if (adapter.itemCount == 0) View.INVISIBLE else View.VISIBLE
    }

    @OnClick(R.id.retryButton)
    fun onRetryClick() {
        Timber.i("onRetryClick")
        loadInitialData()
    }

    // RecyclerView Adapter

    internal class Adapter(context: Context) : RecyclerView.Adapter<ViewItemHolder>() {

        private val mDiffer = AsyncListDiffer<Models.Item>(this, DIFF_CALLBACK)
        private val layoutInflater = LayoutInflater.from(context)

        init {
            setHasStableIds(true)
        }

        fun submitList(value: List<Models.Item>?) {
            value?.let {
                mDiffer.submitList(it)
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewItemHolder {
            val view = layoutInflater.inflate(R.layout.property_listing_item_view, parent, false)
            return ViewItemHolder(view)
        }

        override fun getItemId(position: Int): Long {
            return mDiffer.currentList[position].advertisementId
        }

        override fun getItemCount(): Int {
            return mDiffer.currentList.size
        }

        override fun onBindViewHolder(holder: ViewItemHolder, position: Int) {
            holder.data = mDiffer.currentList[position]
        }

        override fun onViewRecycled(holder: ViewItemHolder) {
            holder.reset()
            super.onViewRecycled(holder)
        }

        companion object {

            val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Models.Item>() {
                override fun areContentsTheSame(oldItem: Models.Item, newItem: Models.Item): Boolean {
                    return oldItem == newItem
                }

                override fun areItemsTheSame(oldItem: Models.Item, newItem: Models.Item): Boolean {
                    return oldItem.advertisementId == newItem.advertisementId
                }
            }
        }

    }

    // ViewPager Adapter

    internal class PagerAdapter(private val pictures: List<String>, private val context: Context) : android.support.v4.view.PagerAdapter() {

        private val layoutInflater = LayoutInflater.from(context)

        private val resources = context.resources

        override fun isViewFromObject(p0: View, p1: Any): Boolean = p0 == p1

        override fun getCount(): Int = pictures.size

        override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
            container.removeView(view as View)
        }

        @SuppressLint("CheckResult")
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view: View = layoutInflater.inflate(R.layout.property_listing_single_image, container, false)
            container.addView(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)


            val picture = pictures[position]
            val imageView: ImageView = view.findViewById(R.id.imageView)
            Picasso.get().cancelRequest(imageView)
            Picasso.get().load(pictures[position]).fit().centerInside()
                    .into(imageView, object : Callback {
                        override fun onSuccess() {
                            val bmp = (imageView.drawable as BitmapDrawable).bitmap
                            BlurBitmapCreator.getInstance(context)
                                    .blurImage(bmp, picture, { result: Bitmap, key: String ->
                                        run {
                                            if (key == picture && imageView.context != null)
                                                imageView.background = BitmapDrawable(resources, result)
                                        }
                                    }, { _: Throwable, key: String ->
                                        run {
                                            if (key == picture && imageView.context != null)
                                                imageView.background = null

                                        }
                                    })

                        }

                        override fun onError(e: Exception?) {}
                    })
            return view
        }

    }

    // RecyclerView Item Holder

    internal class ViewItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @BindView(R.id.cardTitle)
        lateinit var titleText: TextView

        @BindView(R.id.cardLocation)
        lateinit var locationText: TextView

        @BindView(R.id.viewPager)
        lateinit var viewPager: ViewPager

        @BindView(R.id.cardPrice)
        lateinit var cardPrice: TextView

        @BindView(R.id.imageButton)
        lateinit var favButton: CheckBox

        @BindView(R.id.pageIndicatorView)
        lateinit var pageIndicator: PageIndicatorView

        var data: Models.Item? = null
            set(value) {
                if (field != value) {
                    field = value
                    update()
                }
            }

        init {
            ButterKnife.bind(this, itemView)

            locationText.setOnClickListener { view ->
                Timber.i("locationText.onClick")
                data?.apply {
                    geoLocation.isEmpty().let { _ ->
                        val gmmIntentUri = Uri.parse("geo:0,0?q=$gmapsLocationQuery($humanAddress)")
                        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                        mapIntent.setPackage("com.google.android.apps.maps")
                        startActivity(view.context, mapIntent, null)
                    }
                }
            }

            viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

                override fun onPageSelected(position: Int) {
                    pageIndicator.selection = position
                }

                override fun onPageScrollStateChanged(state: Int) {}
            })
        }

        @SuppressLint("SetTextI18n")
        private fun update() {
            data?.let {
                titleText.text = it.title
                locationText.text = it.humanAddress
                cardPrice.text = it.monthlyPrice
                cardPrice.visibility = if (it.price > 0) View.VISIBLE else View.INVISIBLE
                favButton.isChecked = Favourites.getInstance(itemView.context).isFavourite(it.advertisementId)

                favButton.setOnCheckedChangeListener { _, checked ->
                    if (checked)
                        Favourites.getInstance(itemView.context).addToFavourites(it.advertisementId)
                    else
                        Favourites.getInstance(itemView.context).removeFromFavourites(it.advertisementId)
                }

                viewPager.adapter = PagerAdapter(it.pictures, itemView.context)
                pageIndicator.count = it.pictures.size
                pageIndicator.visibility = if (it.pictures.size > 1) View.VISIBLE else View.INVISIBLE
            }; run {
            }

        }

        fun reset() {
            favButton.setOnCheckedChangeListener(null)
        }
    }

}
