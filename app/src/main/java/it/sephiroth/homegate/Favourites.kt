package it.sephiroth.homegate

import android.content.Context
import android.content.SharedPreferences
import timber.log.Timber

class Favourites private constructor(context: Context) : SharedPreferences.OnSharedPreferenceChangeListener {
    private val preferences: SharedPreferences = context.getSharedPreferences("homegate_listing", 0)

    init {
        preferences.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onSharedPreferenceChanged(p0: SharedPreferences?, key: String?) {
        key?.let {
            if (it.startsWith("fav_")) {
            }
        }
    }

    fun clearAll() {
        preferences.edit().clear().apply()
    }

    fun addToFavourites(id: Long) {
        Timber.i("addToFavs: $id")
        preferences.edit().putBoolean("fav_" + id.toString(), true).apply()
    }

    fun removeFromFavourites(id: Long) {
        Timber.i("removeFromFavs: $id")
        preferences.edit().remove("fav_" + id.toString()).apply()
    }

    fun isFavourite(id: Long): Boolean {
        return preferences.getBoolean("fav_" + id.toString(), false)
    }

    companion object {

        @Volatile
        private var instance: Favourites? = null

        fun getInstance(context: Context): Favourites {
            val i = instance
            if (i != null) {
                return i
            }

            return synchronized(this) {
                val i2 = instance
                if (i2 != null) {
                    i2
                } else {
                    val created = Favourites(context)
                    instance = created
                    created
                }
            }
        }
    }

}